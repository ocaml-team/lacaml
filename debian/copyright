Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lacaml
Upstream-Contact: Markus Mottl <markus.mottl@gmail.com>
Source: https://bitbucket.org/mmottl/lacaml

Files: *
Copyright: Markus Mottl <markus.mottl@gmail.com>
           Christophe Troestler <Christophe.Troestler@umons.ac.be>
           Egbert Ammicht <eammicht@lucent.com>
           Patrick Cousot <Patrick.Cousot@ens.fr>
           Sam Ehrlichman <sehrlichman@janestreet.com>
           Liam Stewart <liam@cs.toronto.edu>
           Oleg Trott <ot14@columbia.edu>
           Martin Willensdorfer <ma.wi@gmx.at>
License: LGPL-2.1 | other
 As a special exception to the GNU Library General Public License,
 you may link, statically or dynamically, a "work that uses the
 Library" with a publicly distributed version of the Library to
 produce an executable file containing portions of the Library, and
 distribute that executable file under terms of your choice, without
 any of the additional requirements listed in clause 6 of the GNU
 Library General Public License.  By "a publicly distributed version
 of the Library", we mean either the unmodified Library as distributed
 by INRIA, or a modified version of the Library that is distributed
 under the conditions defined in clause 3 of the GNU Library General
 Public License.  This exception does not however invalidate any other
 reasons why the executable file might be covered by the GNU Library
 General Public License.
 .
 Lacaml is distributed under the terms of the GNU Lesser General
 Public License version 2.1, with the special exception to it reported
 above.
 .
 The full text of the GNU Lessere General Public License version 2.1
 can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: debian/*
Copyright: 2013 Lifeng Sun <lifongsun@gmail.com>
License: GPL-3+
  The Debian packaging is copyright Lifeng Sun, and licensed
  under the GNU General Public License (version 3 or above), see
  `/usr/share/common-licenses/GPL-3' for the full text.
